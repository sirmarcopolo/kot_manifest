# Manifest

Zebraliśmy się, żeby ostatecznie określić linię, gdzie to szaleństwo powinno się zakończyć. Szaleństwo prawne, medyczne, szkolne, biznesowe, rolnicze.  Każde z tych środowisk odbiera niepokojąco plany rządu, które uderzają w nas wszystkich z różnych stron. Poprzez brak dostępu do opieki medycznej, prawa do szkolnictwa, własności prywatnej, dysponowania swoim zdrowiem, dostępu do wymiaru sprawiedliwości. Wszystko zostało nam odebrane. Dokładnie wszytko. Zabrali nam pieniądze z OFE - wszystkie. Zabrali oszczędności, swobody obwatelskie, prawa. W trochę ponad rok. 

Aby ocalić to, co nas łączy, a co zostanie określone poniżej. 

Zebraliśmy się, żeby powiedzieć sobie, że jest nas wielu. 

Zebraliśmy się, żeby powiedzieć dość.

## Deklaracja
### Wolność
Jestem za wolnością jednostki z całą jej mocą dycycyjną w danym punkcie w czaso-przestrzeni. 

W szczególności każdy ma prawo powiedzieć, co mu się wydaje i nikt nie ma prawa go wyłączać.
Każdy ma prawo odpowiedzieć z takimi prawami.

Wolność człowieka to wolność wymiany myśli.

> Wolność oznacza prawo do twierdzenia, że dwa i dwa to cztery. Z niego wynika reszta.

Jestem za:
- wolnością wymiany myśli w internecie 
- jestem za otwartą dyskusją bez zmieważania 
- jestem za wolnością w sposobie leczenia-przyjmuję jednocześnie odpowiedzlaność za swoje decyzje
- jestem przeciwny cenzurze w jakiejkolwiek formie. 
- jestem za wolnością religijną. Każdy z nas ma prawo wierzyć, w co chce, kiedy chce i jak chce.
- jestem za wolnością gospodarczą . Jestem również świadomy dysfunkcji panujących w obecnej wersji.
- jestem przeciwny segregacji ludzi bez względu na okoliczności.

### Sprawiedliwość

Prawo dotyczy każdego z nas tak samo. Nie ważne czy jesteś politykiem, sędzią, czarny, biały, z Meksyku, Żydem, Arabem, Polakiem czy kim tam chcesz być. Prawo jednak dotyczy Cie tak samo. 

- Nie ma równych i równiejszych. Odpowiadamy tak samo za swoje czyny.
- Chcę mieć pewność, że każda sprawa, która posiada te same parametry- będzie miała taki sam wynik w sądzie.


### Własność

Każdy ma prawo dysponować i posiadać majątek zgodnie ze swoim sumieniem i możliwościami. 

- Nikt nie ma prawa ograniczać nikomu możliwości zarabiania i gromaczenia majątku
- Nikt nie ma prawa odebrania obywatelom prawa do gotówki jako formy własności. 
- Własność stanowi siłę. 
- Biedniejsze społeczeństwo łatwiej ubezwłasnowolnić.

### Państwo i polityka

Państwo i obywatele danego kraju to podstawa wspólnoty, którą musimy budować.

- Jestem przeciwny atakom na obywateli państwa Polskiego.
- Jestem przeciwny wojnie polsko-polskiej. 
- Jestem przeciwny bezkarności klasy politycznej i aparatu państwowego. 
- Jestem za rozliczeniem wszystkich polityków rządzących od 89 roku wraz z etapem transformacji.
- Jestem za ideą demokracji płynnej lub innej idei umożliwiającej obywatelom mieć realny głos w aktualnych sprawach. 
- Nie zgadzam się w tym, że głos raz dany nie może być zabrany szybciej niż za 4 lata.
- Jestem za otwartym dostępem do danych obywatela przez systemy państwowe.

### Strefa wirusów 

W dobie szaleństwa medialnego i politycznego domagamy się , co następuje:

- obiektywnej 
